﻿using System;

namespace Task7
{
    class Program
    {
        static void Main(string[] args)
        {
            int number1 = 1;
            int number2 = 2;
            int number3 = number1 + number2;
            int sum = 2;
            while (number3< 4000000)
            {
                number1 = number2;
                number2 = number3;
                number3 = number1 + number2;
                if (number3 % 2 == 0)
                {
                    sum = sum + number3;
                }
            }
            Console.WriteLine("The sum is {0}", sum);
        }
    }
}